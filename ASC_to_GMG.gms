GLOBAL_MAPPER_SCRIPT VERSION="1.0"
//Skrypt przetwarzaj�cy dane *.ASC na *.GMG w folderze
//Autor: Andrzej Michalski

//Aby dopasowa� dane do aruksza operujemy nast�puj�cymi zmiennymi
//Warto�� VAL_STOP ustawiamy o 1 wi�ksz� ni� ostatnia liczba god�a arkusza setki
//np je�eli ostatni arkusz z M-34-... to np M-34-102 to wpisujemy VAL_STOP=103

//Poniewa� arkusze s� pogrupowane folderami dla ka�dego arkusza w wyra�eniu IMPORT i EXPORT
//Musimy zmieni� warto�� M-.. na np. M-34-, M-33- w zale�no��i od arkusza

//Pami�tamy aby w komendach IMPORT i EXPORT mie� poprawn� �cie�k� do folder�w

// Skrypt dzia�a tylko dla jednej grupy map np. M-33, M-34 aby przeliczy� now� grup� trzeba otworzy� nowy skrypt

VAR_LOOP_START VAL_START=1 VAL_STOP=200 VAL_STEP=1 VAR_NAME="%ARK%"
	IMPORT_DIR_TREE DIRECTORY="D:\Andrzej\scripting_data\proba\M-34-%ARK%" FILENAME_MASKS="*.ASC"
	EXPORT_ELEVATION FILENAME="D:\Andrzej\scripting_data\proba\M-34-%ARK%.gmg" TYPE=GLOBAL_MAPPER_GRID
	UNLOAD_ALL



VAR_LOOP_END

//W razie powo�ywania si� na skrypt prosz� o afiliacj� "Michalski A., 2017 - Skrypt przetwarzaj�cy *.ASC na *.GMG. PIG-PIB, Krak�w."  


